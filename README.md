# 基于软件仓库的分布式开发环境集成平台

## 简介：
一个基于Eclipse Che的集成开发环境，主要用于对以软件仓库为中心的智能化工具进行有效的集成，以供开发者进行开发。

## 特性：
 集成北京大学 Cart 组件
 集成复旦大学 CLDIFF
 集成复旦大学 CodeWisdom
 集成南京大学 PGS
 集成南京大学 MLB

## 架构：
基于Eclipse CHE version 6.16.0


## 运行说明：

cd to the project folder mvn clean install

Run this sample by mounting assembly to your Che Docker image:

docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v {data_folder}:/data -v {project_absolute_path}/assembly/assembly-main/target/eclipse-che-6.16.0/eclipse-che-6.16.0:/assembly -v /root/.m2:/home/user/.m2 eclipse/che:6.16.0 start

Example of running this command docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v /root/che.data.16:/data -v /root/qyd-ide-server-extension/assembly/assembly-main/target/eclipse-che-6.16.0/eclipse-che-6.16.0:/assembly -v /root/.m2:/home/user/.m2 eclipse/che:6.16.0 start

## 页面展示：
![avatar](./ExamplePics/Example01.png)
<=TO BE CONTINUE =]

## 联系我们：
Name：朱老师
Mobile Phone：13917590652
Email：zhu_hongming@tongji.edu.cn
